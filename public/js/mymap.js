/*
** Copyright 2014 Google Inc.
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**    http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
requirejs.config({
  paths: {
    'sosomaps': '/js/sosomaps',
	'googlemaps': '/js/googlemaps',
	'config': '/js/config',
	'jquery': '/js/lib/jquery/jquery-2.0.3.min',
    'jquery-private': '/js/jquery-private'
  },
  shim: {
    'config': { exports: 'config' },
    'sosomaps': {
     deps: [
		'async!http://map.qq.com/api/js?v=2.exp&libraries=geometry&key=S5PBZ-MX53W-2AHRY-R7Y5S-WTCW6-OLFRF&sensor=false!callback'
		]
    },
	'googlemaps': {
      deps: [
        'async!http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry&sensor=false!callback'
      ]
    }
  },
  map: {
    '*': { 'jquery': 'jquery-private' },
    'jquery-private': { 'jquery': 'jquery' }
  }
});

define(['sosomaps','googlemaps','config','jquery'], function(QMaps,GMaps,config,$) {
   var apiProvider = config.provider+1;    //1 google 2 tencent  测试用
   var tencentKey = config.tencentKey;
   function SetProvider(providerID) {
     apiProvider = providerID;
   }
   
   function StreetViewService() {
      var sv_svc;
	  if(apiProvider==1) 
		sv_svc = new GMaps.StreetViewService();
	  else if(apiProvider==2) 
		sv_svc = new QMaps.PanoramaService();
      return sv_svc;
   }
   
   function computeOffset(from, distance, heading)
   {
     var loc;
     if(apiProvider==1) 
	    loc = GMaps.geometry.spherical.computeOffset(from, distance, heading);
	 else if(apiProvider==2) 
	    loc = QMaps.geometry.spherical.computeOffset(from, distance, heading);
	 return loc;
   }
  
   function LatLng(lat,lng)
   {
     var loc;
	 if(apiProvider==1)
	    loc = new GMaps.LatLng(lat,lng);
	 else if(apiProvider==2) 
		loc = new QMaps.LatLng(lat,lng);
	 return loc;
   }
   
   function Point(x,y)
   {
     var point;
	 if(apiProvider==1)
		point = new GMaps.Point(x,y);
	 else if(apiProvider==2) 
		point = new QMaps.Point(x,y);
	 return point;
   }
   
   function Size(width,height)
   {
     var size;
	 if(apiProvider==1)
		size = new GMaps.Size(width,height);
	 else if(apiProvider==2) 
		size = new QMaps.Size(width,height);
	 return size;
   }
   
   function Map(div,opt)
   {
      var map;
	  if(apiProvider==1)
		map = new GMaps.Map(div,opt);
	 else if(apiProvider==2) 
		map = new QMaps.Map(div,opt);   
	  
	  return map;
   }
   
   function StreetViewPanorama(div,opt)
   {
     var streetview;
	 if(apiProvider==1)
		streetview = new GMaps.StreetViewPanorama(div,opt);
     else if(apiProvider==2) 
	    streetview = new QMaps.Panorama(div,opt); 
     return streetview;
   }
   
   function addListener(instance, eventName, handler)
   {
	   if(apiProvider==1)
	     GMaps.event.addListener(instance, eventName,handler);
	   else if(apiProvider==2) 
		 QMaps.event.addListener(instance, eventName,handler);
   }
   
   function addListenerOnce(instance, eventName, handler)
   {
      if(apiProvider==1)
		GMaps.event.addListenerOnce(instance, eventName,handler);
	  else if(apiProvider==2) 
		QMaps.event.addListenerOnce(instance, eventName,handler);
   }
   
   function Marker(opt)
   {
      var marker;
      if(apiProvider==1)
		marker = new GMaps.Marker(opt);
	  else if(apiProvider==2) 
        marker = new QMaps.Marker(opt);
	  return marker;
   }
   
   function InfoWindow(opt)
   {
      var info; 
	  if(apiProvider==1)
		info = new GMaps.InfoWindow({
        content: opt.content,
        disableAutoPan: opt.disableAutoPan
      });
	  else if(apiProvider==2)
		info = new QMaps.InfoWindow({
        content: opt.content,
		map: opt.map
      });
	  return info;
   }
   
   function trigger(instance, eventName)
   {
     if(apiProvider==1)
       GMaps.event.trigger(instance, eventName);
	 else if(apiProvider==2) 
	   QMaps.event.trigger(instance, eventName);
   }
  
   
   function getPanoramaByLocation(position, radius, cb)
   {
      if(apiProvider==1) 
	  {
	    var sv_svc = new GMaps.StreetViewService();
		sv_svc.getPanoramaByLocation(position,radius,cb);
	  }
	  else if(apiProvider==2)
	  {
	    var sv_svc = new QMaps.PanoramaService();
		sv_svc.getPano(position,radius,cb);
	  }
   }
   
   function getPanoramaById(panoid,cb)
   {
      if(apiProvider==1) 
	  {
	    var sv_svc = new GMaps.StreetViewService();
		sv_svc.getPanoramaById(panoid,cb);
	  }
	  else if(apiProvider==2) 
	  {
	    var data = null;
	    if (panoid.match(/^\w+$/))
	    {
	      $.getJSON("http://apis.map.qq.com/ws/streetview/v1/getpano?id="+panoid+"&radius=100&key="+tencentKey+"&output=jsonp&callback=?",
	      function(ret) {
		  if(ret.status == 0)
		  {
			data = {svid:ret.detail.id,latlng: new QMaps.LatLng(ret.detail.location.lat,ret.detail.location.lng),description:ret.detail.description};
		    cb(data);
		  }	 
	     });	
	    }
	  }
   }
   
   function MapTypeId()
   {
     if(apiProvider==1) 
      return GMaps.MapTypeId;
	  else if(apiProvider==2)
	  return QMaps.MapTypeId;
   }
   
   function ControlPosition()
   {
     if(apiProvider==1) 
      return GMaps.ControlPosition;
	 else if(apiProvider==2)
	  return QMaps.ControlPosition;
   }
   
   function disableDefaultUI(map,mystyle)
   {
    if(apiProvider==1) 
	{
	if(mystyle!="")
       map.setOptions({styles: mystyle,disableDefaultUI: true});
	 else
	  map.setOptions({disableDefaultUI: true});
	}
	else if(apiProvider==2)
	{
	  map.setOptions({navigationControl: false,
		scaleControl: false,
		panControl: false,
		zoomControl: false,
		mapTypeControl:false});
	 }
   }
   
   function disableSVDefaultUI()
   {
   
    var svOptions;
    if(apiProvider==1) 
	{
	  svOptions = {
        visible: true,
        disableDefaultUI: true
      };
	}
	else if(apiProvider==2)
	{
	  svOptions = {
	    visible: true,
        disableCompass: true,
		disableMove:true,
		//scrollwheel:false
	  };
	}

	 return svOptions;
   }
   
   function enableSVDefaultUI(svOptions)
   {
     if(apiProvider==1)
	 {
	    svOptions.linksControl = true;
	 }
     else if(apiProvider==2)
	 {
	    svOptions.linksControl = true;
		svOptions.disableCompass = false;
		svOptions.disableMove = false;
	 }
   }
   
   function StreetViewCoverageLayer()
   {
     var sv_coverage_layer;
     if(apiProvider==1) 
		sv_coverage_layer = new GMaps.StreetViewCoverageLayer();
	 else if(apiProvider==2)
	    sv_coverage_layer = new QMaps.PanoramaLayer()
	 return sv_coverage_layer; 
   }
   
   function setHdgIcon(headingIndex)
   {
     var markerIcon;
     if(apiProvider==1) 
	 {
	    var sWidth = 56.75*parseInt(headingIndex%4);
	    var sHeight = 56.75*parseInt(headingIndex/4);
		markerIcon = {
	      url: 'icons/sv_markers.png',
          // This marker is 56.75 pixels wide by 56.75 pixels tall.
          size: new GMaps.Size(56.75,56.75),
          // The origin for this image is sWidth,sHeight.
          origin: new GMaps.Point(sWidth,sHeight),
          // The anchor for this image is the base of the flagpole at 56.75/2,40.
          anchor: new GMaps.Point(56.75/2,40)
	   };
	 }
	 else if(apiProvider==2)
	 {
	    var sWidth = 56.67*parseInt(headingIndex%3);
		var sHeight = 56.75*parseInt(headingIndex/3);
		markerIcon = new QMaps.MarkerImage(
		"icons/sv_soso_markers.png",
		new QMaps.Size(56.67,56.75),
		new QMaps.Point(sWidth,sHeight),
		new QMaps.Point(56.67/2,32)
		);
	 }
	 return markerIcon;
   }
   
   function otherSet(streetview,map,zoom,mymode)
   {
     if(apiProvider==1) 
	 {
	    streetview.setOptions({ mode: mymode });  
       // *** apply the custom streetview object to the map
        map.setStreetView( streetview );
	 }
	 else if(apiProvider==2) 
	 {
	   streetview.setZoom(zoom);
	 }
   }
   
   function dataProcess(panodata)
   {
     if(apiProvider==1) 
	 {
	   panodata.latlng = panodata.location.latLng;
       panodata.svid = panodata.location.pano;
	 }
   }
   
   function MarkerIndex(heading)
   {
     var nindex;
     if(apiProvider==1)
	   nindex = parseInt(heading/22.5)%16;
	 else if(apiProvider==2)
	   nindex = parseInt(heading/30)%12;
	  return nindex;
   }
   
   function DefaultCenter(centerarr)
   {
     var center = new Array();;
     if(apiProvider==1)
	 {
	   center[0] = centerarr[0].lat;
	   center[1] = centerarr[0].lng;
	 }
	 else if(apiProvider==2)
	 {
	   center[0] = centerarr[1].lat;
	   center[1] = centerarr[1].lng;
	  }
	  return center;
   }
   
   function DeletePoi(poiArr)
   {
      var nPoiArr = poiArr;
	  
	  for(var i = 0; i < poiArr.length; ++i)
	  {
	    var nPoiSubArr = new Array();
	   for(var j = 0; j < poiArr[i].objects.length; ++j)
	   {
	     if((poiArr[i].objects[j].identifier).match(/^[\w-]{22}$/)&&apiProvider==1)
		 {
		    nPoiSubArr.push(poiArr[i].objects[j]);
		 }
		 else if(!(poiArr[i].objects[j].identifier).match(/^[\w-]{22}$/)&&apiProvider==2)
		 {
		    nPoiSubArr.push(poiArr[i].objects[j]);
		 }
	   }
	    nPoiArr[i].objects = nPoiSubArr;
	  }
	  return nPoiArr;
   }
   
  return{
	StreetViewService: StreetViewService,
	computeOffset: computeOffset,
	LatLng: LatLng,
	Size: Size,
	Point: Point,
	Map: Map,
	StreetViewPanorama: StreetViewPanorama,
	addListener: addListener,
	addListenerOnce: addListenerOnce,
	Marker: Marker,
	MapTypeId: MapTypeId,
	ControlPosition: ControlPosition,
	InfoWindow: InfoWindow,
	trigger: trigger,
	getPanoramaByLocation: getPanoramaByLocation,
	getPanoramaById: getPanoramaById,
	disableDefaultUI: disableDefaultUI,
	disableSVDefaultUI: disableSVDefaultUI,
	enableSVDefaultUI: enableSVDefaultUI,
	StreetViewCoverageLayer:StreetViewCoverageLayer,
	setHdgIcon:setHdgIcon,
	otherSet:otherSet,
	dataProcess:dataProcess,
	MarkerIndex:MarkerIndex,
	DefaultCenter:DefaultCenter,
	SetProvider:SetProvider,
	DeletePoi:DeletePoi,
  }
});

